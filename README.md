## Loopme test webapp

Requirements: 
* Java 8
* maven

To run webapp execute ```mvn tomcat7:run``` and launch app by http://localhost:8090/

By default two users are present in database:
* admin (ADMIN role)
* adops (ADOPS role)
* pub (PUBLISHER role)

Password = user name

TODO:
* Exception handling (Spring MVC controller advice + angular interceptors + bootstrap alert)
* Cover services with unit tests (parametrized + Mockito)
* Add production webpack config (minified css and js to separate bundles)
* Add custom login form (Spring security config + html fragment) 
* Add i18n (Message source + update angular LabelService)
* Add spinner on loading (angular $http configuration + component)
