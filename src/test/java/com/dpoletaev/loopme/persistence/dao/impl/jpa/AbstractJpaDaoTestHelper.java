package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.github.database.rider.core.DBUnitRule;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@Transactional
@ContextConfiguration(locations = {"classpath:/spring/jpa-dao-test-context.xml"})
@ActiveProfiles({"h2"})
public abstract class AbstractJpaDaoTestHelper {

    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    @Autowired
    EntityManager entityManager;

    @Autowired
    ApplicationContext applicationContext;

    @Rule
    public DBUnitRule dbUnitRule = DBUnitRule.
            instance(() -> dataSource.getConnection());

}
