package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.UserDAO;
import com.dpoletaev.loopme.persistence.dao.UserRoleDAO;
import com.dpoletaev.loopme.persistence.model.UserDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa.UserRoleName;
import com.github.database.rider.core.api.dataset.DataSet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static com.dpoletaev.loopme.persistence.dao.impl.jpa.TestEntity.*;
import static org.assertj.core.api.Assertions.assertThat;

public class TestUserDAOImpl extends AbstractJpaDaoTestHelper {

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserRoleDAO userRoleDAO;


    @Test
    @DataSet(value = {"dataset/json/users.json"})
    public void testCreate() throws Exception {
        //GIVEN
        UserDa user = new UserDa();
        user.setName(TestEntity.USER_NAME);
        user.setEmail(TestEntity.USER_EMAIL);
        user.setRole(userRoleDAO.findById(3));

        //WHEN
        userDAO.create(user);

        //THEN
        assertThat(userDAO.findById(user.getId())).isNotNull();
        assertThat(userDAO.findById(user.getId()).getRole()).isEqualTo(ROLE_PUBLISHER);
    }

    @Test
    @DataSet("dataset/json/users.json")
    public void testFindById() throws Exception {
        //WHEN
        UserDa user = userDAO.findById(2);

        //THEN
        assertThat(user).isEqualTo(user());
    }

    @Test
    @DataSet("dataset/json/users.json")
    public void testFindByUsername() throws Exception {
        //WHEN
        UserDa user = userDAO.findByUserName(USER_NAME);

        //THEN
        assertThat(user).isEqualTo(user());
    }


    @Test
    @DataSet("dataset/json/users.json")
    public void testFindByRolesIds() throws Exception {
        //GIVEN
        List<Integer> userRoleIds = Arrays.asList(2, 3);

        //WHEN
        List<UserDa> users = userDAO.findByRolesIds(userRoleIds);

        //THEN
        assertThat(users).hasSize(2);
    }


    @Test
    @DataSet("dataset/json/users.json")
    public void testDelete() {
        //GIVEN
        UserDa userDa = userDAO.findById(2);

        //WHEN
        userDAO.delete(userDa);

        //THEN
        UserDa actual = userDAO.findById(2);
        assertThat(actual).isNull();
    }
}