package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.AppDAO;
import com.dpoletaev.loopme.persistence.dao.ContentTypeDAO;
import com.dpoletaev.loopme.persistence.dao.UserDAO;
import com.dpoletaev.loopme.persistence.model.AppDa;
import com.dpoletaev.loopme.persistence.model.ContentTypeDa;
import com.dpoletaev.loopme.persistence.model.UserDa;
import com.github.database.rider.core.api.dataset.DataSet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.dpoletaev.loopme.persistence.dao.impl.jpa.TestEntity.app;
import static org.assertj.core.api.Assertions.assertThat;

public class TestAppDAOImpl extends AbstractJpaDaoTestHelper {

    @Autowired
    private AppDAO appDAO;

    @Autowired
    private ContentTypeDAO contentTypeDAO;

    @Autowired
    private UserDAO userDAO;

    @Test
    @DataSet({"dataset/json/contentTypes.json", "dataset/json/users.json"})
    public void testCreate() throws Exception {
        //GIVEN
        List<ContentTypeDa> contentTypes = contentTypeDAO.findAll();
        UserDa user = userDAO.findById(1);
        AppDa app = app(user, new HashSet<>(contentTypes));

        //WHEN
        appDAO.create(app);

        //THEN
        AppDa actual = appDAO.findById(app.getId());
        assertThat(actual).isNotNull();
        assertThat(actual.getContentTypes()).hasSameSizeAs(contentTypes);
        assertThat(actual.getUser()).isEqualTo(user);
    }


    @Test
    @DataSet({"dataset/json/users.json", "dataset/json/apps.json"})
    public void testFindByUserId() throws Exception {
        //GIVEN
        Integer userId = 1;
        int expectedSize = 2;

        //WHEN
        List<AppDa> actual = appDAO.findByUserId(userId);

        //THEN
        assertThat(actual).hasSize(expectedSize);
    }
}