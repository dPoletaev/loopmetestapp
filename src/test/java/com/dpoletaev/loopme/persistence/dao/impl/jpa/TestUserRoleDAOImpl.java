package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.UserRoleDAO;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;
import com.github.database.rider.core.api.dataset.DataSet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class TestUserRoleDAOImpl extends AbstractJpaDaoTestHelper{

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Test
    @DataSet("dataset/json/userRoles.json")
    public void testFindAll() throws Exception {
        //GIVEN
        int expectedSize = 3;

        //WHEN
        List<UserRoleDa> userRoles = userRoleDAO.findAll();

        //THEN
        assertThat(userRoles).hasSize(expectedSize);
    }
}