package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.model.AppDa;
import com.dpoletaev.loopme.persistence.model.ContentTypeDa;
import com.dpoletaev.loopme.persistence.model.UserDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa.UserRoleName;

import java.util.Collection;
import java.util.Set;

public final class TestEntity {

    public static final UserRoleDa ROLE_ADMIN = new UserRoleDa(UserRoleName.ADMIN);
    public static final UserRoleDa ROLE_ADOPS = new UserRoleDa(UserRoleName.ADOPS);
    public static final UserRoleDa ROLE_PUBLISHER = new UserRoleDa(UserRoleName.PUBLISHER);

    public static final int USER_ID = 2;
    public static final String USER_NAME = "user2";
    public static final String USER_EMAIL = "mail2@mail.com";
    public static final String APP_NAME = "appName";


    public static UserDa user() {
        UserDa user = new UserDa();
        user.setId(USER_ID);
        user.setEmail(USER_EMAIL);
        user.setName(USER_NAME);
        user.setRole(ROLE_PUBLISHER);
        return user;
    }

    public static AppDa app(UserDa user, Set<ContentTypeDa> contentTypes) {
        AppDa app = new AppDa();
        app.setUser(user);
        app.setName(APP_NAME);
        app.setAppType(AppDa.AppTypeDa.ANDROID);
        app.setContentTypes(contentTypes);
        return app;
    }

    private TestEntity() {
        throw new AssertionError("Utility clazz!");
    }


}
