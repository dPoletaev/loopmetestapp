create table USER_ROLES (ID integer not null, NAME varchar(255), primary key (ID));
create table USERS (ID integer not null, EMAIL varchar(255), NAME varchar(255), ROLE_ID integer not null, primary key (ID));

create table ID_GEN (GEN_KEY varchar(255) not null, GEN_VALUE bigint, primary key (GEN_KEY));

create table CONTENT_TYPES (ID integer not null, NAME varchar(255), primary key (ID));