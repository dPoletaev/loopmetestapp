package com.dpoletaev.loopme.util;

import com.dpoletaev.loopme.dto.*;
import com.dpoletaev.loopme.persistence.model.AppDa;
import com.dpoletaev.loopme.persistence.model.ContentTypeDa;
import com.dpoletaev.loopme.persistence.model.UserDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa.UserRoleName;
import org.apache.commons.lang3.Validate;

import java.util.stream.Collectors;

public final class ModelConvertUtil {

    public static AppUser convert(UserDa userDa) {
        AppUser user;

        if (userDa == null) {
            user = null;
        } else {
            user = new AppUser();
            user.setId(userDa.getId());
            user.setName(userDa.getName());
            user.setEmail(userDa.getEmail());

            Validate.notNull(userDa.getRole());
            user.setRole(convert(userDa.getRole()));
        }
        return user;
    }

    public static UserDa convert(AppUser appUser) {
        UserDa userDa = new UserDa();
        userDa.setId(appUser.getId());
        userDa.setName(appUser.getName());
        userDa.setEmail(appUser.getEmail());
        userDa.setRole(convert(appUser.getRole()));
        return userDa;
    }

    public static UserRoleDa convert(UserRole userRole) {
        return new UserRoleDa(userRole.getId(), convert(userRole.getName()));
    }

    public static UserRole convert (UserRoleDa userRoleDa) {
        return new UserRole(userRoleDa.getId(), convert(userRoleDa.getName()));
    }

    public static UserRole.UserRoleName convert(UserRoleDa.UserRoleName userRoleName) {
        return UserRole.UserRoleName.valueOf(userRoleName.name());
    }

    public static UserRoleDa.UserRoleName  convert(UserRole.UserRoleName userRoleName) {
        return UserRoleDa.UserRoleName.valueOf(userRoleName.name());
    }

    public static App convert(AppDa appDa) {
        if(appDa == null) {
            return null;
        }

        App app = new App();
        app.setId(appDa.getId());
        app.setName(appDa.getName());
        app.setAppType(convert(appDa.getAppType()));
        app.setContentTypes(appDa.getContentTypes().stream()
                .map(ModelConvertUtil::convert)
                .collect(Collectors.toSet())
        );
        app.setUser(convert(appDa.getUser()));

        return app;
    }

    public static AppDa convert(App app) {
        if(app == null) {
            return null;
        }

        AppDa appDa = new AppDa();
        appDa.setId(app.getId());
        appDa.setName(app.getName());
        appDa.setAppType(convert(app.getAppType()));
        appDa.setUser(convert(app.getUser()));
        appDa.setContentTypes(app.getContentTypes().stream()
        .map(ModelConvertUtil::convert)
        .collect(Collectors.toSet()));

        return appDa;
    }


    public static AppType convert(AppDa.AppTypeDa appTypeDa) {
        return AppType.valueOf(appTypeDa.name());
    }

    public static AppDa.AppTypeDa convert(AppType appType) {
        return AppDa.AppTypeDa.valueOf(appType.name());
    }

    public static ContentTypeDa convert(ContentType contentType) {
        ContentTypeDa contentTypeDa = new ContentTypeDa();
        contentTypeDa.setId(contentType.getId());
        contentTypeDa.setContentTypeName(ContentTypeDa.ContentTypeName.valueOf(contentType.getContentTypeName().name()));
        return contentTypeDa;
    }

    public static ContentType convert(ContentTypeDa contentTypeDa) {
        ContentType contentType = new ContentType();
        contentType.setId(contentTypeDa.getId());
         contentType.setContentTypeName(ContentType.ContentTypeName.valueOf(contentTypeDa.getContentTypeName().name()));
        return  contentType;
    }


    private ModelConvertUtil() {
        throw new AssertionError("Utility clazz");
    }
}
