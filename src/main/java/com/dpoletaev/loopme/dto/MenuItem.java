package com.dpoletaev.loopme.dto;

import lombok.Data;

@Data
public class MenuItem {
    private String label;
    private String url;
    private boolean isSelected = false;

    public MenuItem(String label, String url) {
        this.label = label;
        this.url = url;
    }
}
