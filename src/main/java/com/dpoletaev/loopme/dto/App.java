package com.dpoletaev.loopme.dto;

import lombok.Data;

import java.util.Set;

@Data
public class App {
    private Integer id;

    private String name;

    private Set<ContentType> contentTypes;

    private AppType appType;

    private AppUser user;
}
