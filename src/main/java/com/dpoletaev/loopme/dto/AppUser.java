package com.dpoletaev.loopme.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
public class AppUser implements Serializable{
    private Integer id;
    private String name;
    private String email;
    private UserRole role;

    public String getRoleName() {
        return role == null ? null : role.getName().name();
    }

}
