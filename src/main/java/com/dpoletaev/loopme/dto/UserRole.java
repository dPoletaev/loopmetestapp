package com.dpoletaev.loopme.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {
    private Integer id;
    private UserRoleName name;

    public enum UserRoleName {
        ADMIN, ADOPS, PUBLISHER;
    }

}
