package com.dpoletaev.loopme.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class AllowedUserRoleActions {
    private UserRole userRole;
    private Set<UserAction> allowedActions;
}
