package com.dpoletaev.loopme.dto;

public enum UserAction {
    CREATE, UPDATE, DELETE
}
