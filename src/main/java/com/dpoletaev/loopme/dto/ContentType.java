package com.dpoletaev.loopme.dto;

import lombok.Data;

@Data
public class ContentType {
    private Integer id;
    private ContentTypeName contentTypeName;

    public enum ContentTypeName {
        VIDEO, IMAGE, HTML
    }
}
