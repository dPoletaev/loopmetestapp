package com.dpoletaev.loopme.dto;

public enum AppType {
    IOS, ANDROID, WEBSITE
}
