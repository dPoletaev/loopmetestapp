package com.dpoletaev.loopme.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppWrapper {

    private App app;

    private boolean editable;

    private boolean deletable;

}