package com.dpoletaev.loopme.persistence.dao;

import com.dpoletaev.loopme.persistence.model.AppDa;

import java.util.List;

public interface AppDAO {

    void create(AppDa app);

    AppDa findById(int id);

    List<AppDa> findByUserId(Integer userId);

    List<AppDa> findAll();

    void delete(AppDa appDa);

    AppDa update(AppDa appDa);

}
