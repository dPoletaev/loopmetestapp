package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.ContentTypeDAO;
import com.dpoletaev.loopme.persistence.model.ContentTypeDa;

import javax.persistence.EntityManager;
import java.util.List;

public class ContentTypeDAOImpl extends AbstractJpaDAO<ContentTypeDa> implements ContentTypeDAO {

    protected ContentTypeDAOImpl(EntityManager entityManager) {
        super(ContentTypeDa.class, entityManager);
    }
}
