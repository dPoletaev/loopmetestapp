package com.dpoletaev.loopme.persistence.dao;

import com.dpoletaev.loopme.persistence.model.UserRoleDa;

import java.util.List;

public interface UserRoleDAO {

    UserRoleDa findById(int id);

    List<UserRoleDa> findAll();
}
