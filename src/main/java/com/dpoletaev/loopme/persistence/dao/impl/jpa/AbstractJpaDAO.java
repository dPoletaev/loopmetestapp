package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractJpaDAO<T extends Serializable> {
    private final Class<T> clazz;

    protected EntityManager entityManager;
    protected CriteriaBuilder criteriaBuilder;

    protected AbstractJpaDAO(Class<T> clazz, EntityManager entityManager) {
        this.clazz = clazz;
        this.entityManager = entityManager;
        this.criteriaBuilder = this.entityManager.getCriteriaBuilder();
    }

    public List<T> findAll() {
        CriteriaQuery<T> criteriaQuery = this.createCriteriaQuery();
        Root<T> root = criteriaQuery.from(this.clazz);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public void create(T entity) {
        entityManager.persist(entity);
    }

    public T findById(int id) {
        return entityManager.find(clazz, id);
    }

    public void delete(T entity) {
        this.entityManager.remove(entity);
    }

    public T update(T entity) {
        return entityManager.merge(entity);
    }

    protected CriteriaQuery<T> createCriteriaQuery() {
        return criteriaBuilder.createQuery(this.clazz);
    }
}
