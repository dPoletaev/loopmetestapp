package com.dpoletaev.loopme.persistence.dao;

import com.dpoletaev.loopme.persistence.model.UserDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;

import java.util.List;

public interface UserDAO {
    void create (UserDa entity);

    UserDa findById(int id);

    UserDa findByUserName(String userName);

    List<UserDa> findByRolesIds(List<Integer> roleIds);

    void delete(UserDa userDa);

    UserDa update(UserDa userDa);
}
