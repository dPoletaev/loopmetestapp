package com.dpoletaev.loopme.persistence.dao;

import com.dpoletaev.loopme.persistence.model.ContentTypeDa;

import java.util.List;

public interface ContentTypeDAO {

    List<ContentTypeDa> findAll();
}
