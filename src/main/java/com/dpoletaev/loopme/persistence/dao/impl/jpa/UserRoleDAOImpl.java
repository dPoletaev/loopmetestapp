package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.UserRoleDAO;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;

import javax.persistence.EntityManager;

public class UserRoleDAOImpl extends AbstractJpaDAO<UserRoleDa> implements UserRoleDAO {
    protected UserRoleDAOImpl(EntityManager entityManager) {
        super(UserRoleDa.class, entityManager);
    }
}
