package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.UserDAO;
import com.dpoletaev.loopme.persistence.model.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import java.util.List;

@Slf4j
public class UserDAOImpl extends AbstractJpaDAO<UserDa> implements UserDAO {

    public UserDAOImpl(EntityManager entityManager) {
        super(UserDa.class, entityManager);
    }

    @Override
    public UserDa findByUserName(String userName) {
        CriteriaQuery<UserDa> criteria = this.createCriteriaQuery();
        Root<UserDa> root = criteria.from(UserDa.class);
        criteria.select(root)
                .where(this.criteriaBuilder.equal(root.get(UserDa_.name), userName));
        try {
            return entityManager.createQuery(criteria).getSingleResult();
        } catch(NoResultException ex) {
            log.debug("User with name {} not found in database", userName);
            return null;
        }
    }

    @Override
    public List<UserDa> findByRolesIds(List<Integer> roleIds) {
        CriteriaQuery<UserDa> criteria = this.createCriteriaQuery();
        Root<UserDa> root = criteria.from(UserDa.class);
        criteria.select(root)
                .where(root.get(UserDa_.role).get(UserRoleDa_.id).in(roleIds));
        return entityManager.createQuery(criteria).getResultList();
    }

    @Override
    public UserDa update(UserDa entity) {
        return super.update(entity);
    }
}
