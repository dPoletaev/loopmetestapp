package com.dpoletaev.loopme.persistence.dao.impl.jpa;

import com.dpoletaev.loopme.persistence.dao.AppDAO;
import com.dpoletaev.loopme.persistence.dao.impl.jpa.AbstractJpaDAO;
import com.dpoletaev.loopme.persistence.model.AppDa;
import com.dpoletaev.loopme.persistence.model.AppDa_;
import com.dpoletaev.loopme.persistence.model.UserDa_;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class AppDAOImpl extends AbstractJpaDAO<AppDa> implements AppDAO {

    protected AppDAOImpl(EntityManager entityManager) {
        super(AppDa.class, entityManager);
    }

    @Override
    public List<AppDa> findByUserId(Integer userId) {
        CriteriaQuery<AppDa> criteriaQuery = this.createCriteriaQuery();
        Root<AppDa> root = criteriaQuery.from(AppDa.class);
        criteriaQuery.select(root)
                .where(this.criteriaBuilder.equal(root.get(AppDa_.user).get(UserDa_.id), userId));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public void delete(AppDa entity) {
        super.delete(entity);
    }

    @Override
    public AppDa update(AppDa entity) {
        return super.update(entity);
    }
}
