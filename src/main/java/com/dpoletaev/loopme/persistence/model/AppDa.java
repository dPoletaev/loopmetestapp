package com.dpoletaev.loopme.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "APPS")
@Getter
@Setter
@ToString
public class AppDa implements Serializable{

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "appGen")
    @TableGenerator(
            name="appGen",
            table="ID_GEN",
            pkColumnName="GEN_KEY",
            valueColumnName="GEN_VALUE",
            pkColumnValue="APP_ID",
            allocationSize=1)
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @ManyToMany
    @JoinTable(
            name = "APP_CONTENT_TYPES",
            joinColumns = @JoinColumn(name = "APP_ID"),
            inverseJoinColumns = @JoinColumn(name = "CONTENT_TYPE_ID")
    )
    private Set<ContentTypeDa> contentTypes;

    @Column(name = "TYPE")
    @Enumerated(value = EnumType.STRING)
    private AppTypeDa appType;

    @ManyToOne(
            fetch = FetchType.LAZY,
            optional = false
    )
    @JoinColumn(name = "USER_ID")
    private UserDa user;


    public enum AppTypeDa {
        IOS, ANDROID, WEBSITE
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof AppDa)) return false;
        AppDa appDa = (AppDa) o;
        return Objects.equals(contentTypes, appDa.contentTypes) &&
                appType == appDa.appType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contentTypes, appType);
    }
}
