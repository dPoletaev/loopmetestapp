package com.dpoletaev.loopme.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "USERS")
@Getter
@Setter
@ToString
public class UserDa implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "userGen")
    @TableGenerator(
            name="userGen",
            table="ID_GEN",
            pkColumnName="GEN_KEY",
            valueColumnName="GEN_VALUE",
            pkColumnValue="USER_ID",
            allocationSize=1)
    private Integer id;

    @Column(name = "NAME", unique = true)
    private String name;

    @ManyToOne(
            fetch = FetchType.LAZY,
            optional = false
    )
    @JoinColumn(name = "ROLE_ID")
    private UserRoleDa role;

    @Column(name = "EMAIL")
    private String email;

    public UserRoleDa.UserRoleName getRoleName() {
        return this.role == null ? null : this.role.getName();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof UserDa)) return false;
        UserDa user = (UserDa) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(email, user.email)
                && Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email);
    }

}
