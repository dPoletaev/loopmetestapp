package com.dpoletaev.loopme.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "USER_ROLES")
@Getter
@Setter
@ToString
public class UserRoleDa implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    @Enumerated(EnumType.STRING)
    @NotNull
    private UserRoleName name;

    public UserRoleDa() {
    }

    public UserRoleDa(Integer id, UserRoleName name) {
        this.id = id;
        this.name = name;
    }

    public UserRoleDa(UserRoleName name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof UserRoleDa)) return false;
        UserRoleDa that = (UserRoleDa) o;
        return name == that.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public enum UserRoleName {
        ADMIN, ADOPS, PUBLISHER
    }
}
