package com.dpoletaev.loopme.persistence.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "CONTENT_TYPES")
@Getter
@Setter
public class ContentTypeDa implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "NAME")
    @Enumerated(value = EnumType.STRING)
    private ContentTypeName contentTypeName;

    public enum ContentTypeName {
        VIDEO, IMAGE, HTML
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof ContentTypeDa)) return false;
        ContentTypeDa that = (ContentTypeDa) o;
        return contentTypeName == that.contentTypeName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contentTypeName);
    }
}
