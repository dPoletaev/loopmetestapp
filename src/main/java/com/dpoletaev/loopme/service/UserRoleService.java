package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.AllowedUserRoleActions;
import com.dpoletaev.loopme.dto.UserAction;
import com.dpoletaev.loopme.dto.UserRole;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Set;

public interface UserRoleService {

    List<AllowedUserRoleActions> getAllowedActions(UserRole role);

    Set<UserAction> getAllowedActions(UserRole subjectRole, UserRole objectRole);

    List<UserRole> getRoles();
}
