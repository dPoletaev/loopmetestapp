package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.ContentType;

import java.util.List;

public interface ContentTypeService {

    List<ContentType> getContentTypes();
}
