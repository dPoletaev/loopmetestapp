package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.AppType;

import java.util.List;

public interface AppTypeService {
    List<AppType> getAppTypes();
}
