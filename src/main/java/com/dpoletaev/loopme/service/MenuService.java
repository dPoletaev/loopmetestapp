package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.MenuItem;
import com.dpoletaev.loopme.dto.AppUser;

import java.util.List;

public interface MenuService {

    List<MenuItem> getMenuItems(AppUser appUser, String currentURL);
}
