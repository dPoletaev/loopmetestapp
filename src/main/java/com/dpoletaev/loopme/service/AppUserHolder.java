package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.AppUser;

public interface AppUserHolder {

    AppUser getUser();
}
