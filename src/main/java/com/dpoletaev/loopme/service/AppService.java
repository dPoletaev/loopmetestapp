package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.App;
import com.dpoletaev.loopme.dto.AppUser;
import com.dpoletaev.loopme.dto.AppWrapper;
import com.dpoletaev.loopme.dto.UserAction;

import java.util.List;
import java.util.Set;

public interface AppService {

    List<AppWrapper> getApps(AppUser appUser);

    AppWrapper create(App app, AppUser appUser);

    void delete(Integer appId);

    AppWrapper edit(App app, AppUser appUser);
}
