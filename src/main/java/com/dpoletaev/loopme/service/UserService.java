package com.dpoletaev.loopme.service;

import com.dpoletaev.loopme.dto.AppUser;

import java.util.List;

public interface UserService {

    AppUser findUserByName(String name);

    List<AppUser> getUsers(AppUser appUser);

    AppUser saveUser(AppUser appUser);

    void deleteUser(Integer id);

    AppUser updateUser(AppUser appUser);
}
