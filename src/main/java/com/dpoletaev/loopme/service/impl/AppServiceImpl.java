package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.*;
import com.dpoletaev.loopme.persistence.dao.AppDAO;
import com.dpoletaev.loopme.persistence.model.AppDa;
import com.dpoletaev.loopme.service.AppService;
import com.dpoletaev.loopme.util.ModelConvertUtil;
import com.dpoletaev.loopme.web.exception.ActionNotAllowedException;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AppServiceImpl implements AppService {
    private AppDAO appDAO;

    public AppServiceImpl(AppDAO appDAO) {
        this.appDAO = appDAO;
    }

    @Transactional
    @Override
    public List<AppWrapper> getApps(final AppUser appUser) {

        List<AppWrapper> result = Collections.emptyList();

        UserRole.UserRoleName userRoleName = appUser.getRole().getName();

        if (userRoleName != UserRole.UserRoleName.ADMIN) {

            result = (userRoleName == UserRole.UserRoleName.ADOPS
                    ? appDAO.findAll()
                    : appDAO.findByUserId(appUser.getId())).stream()
                    .map(ModelConvertUtil::convert)
                    .map(app -> {
                        boolean isOwnApp = app.getUser().getId().equals(appUser.getId());
                        return new AppWrapper(app, isOwnApp, isOwnApp);
                    })
                    .collect(Collectors.toList());
        }

        return result;
    }

    @Transactional
    @Override
    public AppWrapper create(App app, AppUser appUser) {
        app.setUser(appUser);
        AppDa appDa = ModelConvertUtil.convert(app);
        appDAO.create(appDa);
        App createdApp = ModelConvertUtil.convert(appDa);

        return new AppWrapper(createdApp, true, true);
    }

    @Transactional
    @Override
    public void delete(Integer appId) {
        AppDa appDa = appDAO.findById(appId);
        appDAO.delete(appDa);
    }

    @Transactional
    @Override
    public AppWrapper edit(App app, AppUser appUser) {
        AppDa appDa = appDAO.findById(app.getId());

        if (!appUser.getId().equals(app.getUser().getId())) {
            throw new ActionNotAllowedException();
        }

        appDa.setContentTypes((app.getContentTypes().stream()
                .map(ModelConvertUtil::convert))
                .collect(Collectors.toSet()));

        appDa.setAppType(ModelConvertUtil.convert(app.getAppType()));
        appDa.setName(app.getName());


        return this.constructAppWrapper(
                ModelConvertUtil.convert(appDAO.update(appDa)),
                appUser);
    }

    private AppWrapper constructAppWrapper(App app, AppUser appUser) {
        boolean canEdit = appUser.getId().equals(app.getUser().getId());
        return new AppWrapper(app, canEdit, canEdit);

    }
}
