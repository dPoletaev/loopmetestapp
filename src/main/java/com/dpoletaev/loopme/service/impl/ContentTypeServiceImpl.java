package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.ContentType;
import com.dpoletaev.loopme.persistence.dao.ContentTypeDAO;
import com.dpoletaev.loopme.service.ContentTypeService;
import com.dpoletaev.loopme.util.ModelConvertUtil;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.stream.Collectors;

public class ContentTypeServiceImpl implements ContentTypeService {
    private ContentTypeDAO contentTypeDAO;

    public ContentTypeServiceImpl(ContentTypeDAO contentTypeDAO) {
        this.contentTypeDAO = contentTypeDAO;
    }

    @Cacheable("contentTypes")
    @Override
    public List<ContentType> getContentTypes() {
        return contentTypeDAO.findAll().stream()
                .map(ModelConvertUtil::convert)
                .collect(Collectors.toList());
    }
}
