package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.AppUser;
import com.dpoletaev.loopme.dto.AppUserDetails;
import com.dpoletaev.loopme.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.transaction.Transactional;

public class UserDetailsServiceImpl implements UserDetailsService {

    private UserService userService;

    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = userService.findUserByName(username);

        if(user == null) {
            throw new UsernameNotFoundException("Username '" + username + "' not found");
        }

        UserDetails userDetails = new AppUserDetails(user);

        return userDetails;
    }


}
