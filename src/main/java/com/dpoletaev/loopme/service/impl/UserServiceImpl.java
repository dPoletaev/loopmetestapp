package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.AllowedUserRoleActions;
import com.dpoletaev.loopme.dto.AppUser;
import com.dpoletaev.loopme.dto.UserRole;
import com.dpoletaev.loopme.persistence.dao.UserDAO;
import com.dpoletaev.loopme.persistence.dao.UserRoleDAO;
import com.dpoletaev.loopme.persistence.model.UserDa;
import com.dpoletaev.loopme.persistence.model.UserRoleDa;
import com.dpoletaev.loopme.service.UserRoleService;
import com.dpoletaev.loopme.service.UserService;
import com.dpoletaev.loopme.util.ModelConvertUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    private UserRoleDAO userRoleDAO;

    private UserRoleService userRoleService;

    public UserServiceImpl(UserDAO userDAO, UserRoleDAO userRoleDAO, UserRoleService userRoleService) {
        this.userDAO = userDAO;
        this.userRoleDAO = userRoleDAO;
        this.userRoleService = userRoleService;
    }

    @Override
    public AppUser findUserByName(String name) {
        return ModelConvertUtil.convert(userDAO.findByUserName(name));
    }


    @Transactional
    @Override
    public List<AppUser> getUsers(AppUser appUser) {
        List<AllowedUserRoleActions> allowedActions = userRoleService.getAllowedActions(appUser.getRole());

        List<Integer> userRolesIds = allowedActions.stream()
                .filter(allowedUserRoleActions -> !allowedUserRoleActions.getAllowedActions().isEmpty())
                .map(AllowedUserRoleActions::getUserRole)
                .map(UserRole::getId)
                .collect(Collectors.toList());


        return userDAO.findByRolesIds(userRolesIds).stream()
                .map(ModelConvertUtil::convert)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public AppUser saveUser(AppUser appUser) {
        UserDa userDa = ModelConvertUtil.convert(appUser);

        UserRoleDa userRoleDa = userRoleDAO.findById(appUser.getRole().getId());
        userDa.setRole(userRoleDa);

        userDAO.create(userDa);
        return ModelConvertUtil.convert(userDa);
    }

    @Transactional
    @Override
    public void deleteUser(Integer id) {
        UserDa userDa = userDAO.findById(id);
        userDAO.delete(userDa);
    }

    @Transactional
    @Override
    public AppUser updateUser(AppUser appUser) {
        UserDa userDa = userDAO.findById(appUser.getId());
        userDa.setRole(ModelConvertUtil.convert(appUser.getRole()));
        userDa.setEmail(appUser.getEmail());
        userDa.setName(appUser.getName());

        return ModelConvertUtil.convert(userDAO.update(userDa));
    }
}
