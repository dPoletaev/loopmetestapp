package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.AppType;
import com.dpoletaev.loopme.service.AppTypeService;

import java.util.Arrays;
import java.util.List;

public class AppTypeServiceImpl implements AppTypeService {
    @Override
    public List<AppType> getAppTypes() {
        return Arrays.asList(AppType.values());
    }
}
