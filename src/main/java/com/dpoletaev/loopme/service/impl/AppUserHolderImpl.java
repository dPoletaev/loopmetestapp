package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.AppUser;
import com.dpoletaev.loopme.dto.AppUserDetails;
import com.dpoletaev.loopme.service.AppUserHolder;
import org.springframework.security.core.context.SecurityContextHolder;

public class AppUserHolderImpl implements AppUserHolder {

    private AppUser appUser;

    @Override
    public AppUser getUser() {
        if(this.appUser == null) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if(principal instanceof AppUserDetails)
            this.appUser = ((AppUserDetails)principal).getUser();
        }
        return this.appUser;
    }
}
