package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.AllowedUserRoleActions;
import com.dpoletaev.loopme.dto.UserAction;
import com.dpoletaev.loopme.dto.UserRole;
import com.dpoletaev.loopme.dto.UserRole.UserRoleName;
import com.dpoletaev.loopme.persistence.dao.UserRoleDAO;
import com.dpoletaev.loopme.service.UserRoleService;
import com.dpoletaev.loopme.util.ModelConvertUtil;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.springframework.cache.annotation.Cacheable;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UserRoleServiceImpl implements UserRoleService {
    private static Set<UserAction> ALL_ACTIONS =
            ImmutableSet.of(UserAction.CREATE, UserAction.UPDATE, UserAction.DELETE);

    private UserRoleDAO userRoleDAO;

    private  Map<UserRole, Map<UserRole, Set<UserAction>>> userRolesAccessMap;

    public UserRoleServiceImpl(UserRoleDAO userRoleDAO) {
        this.userRoleDAO = userRoleDAO;
    }

    public void init() {
        Map<UserRoleName, UserRole> userRoleNameToRoleMap = this.getRoles().stream()
                .collect(Collectors.toMap(UserRole::getName, Function.identity()));
        UserRole admin = userRoleNameToRoleMap.get(UserRoleName.ADMIN);
        UserRole adops = userRoleNameToRoleMap.get(UserRoleName.ADOPS);
        UserRole publisher = userRoleNameToRoleMap.get(UserRoleName.PUBLISHER);

        userRolesAccessMap = ImmutableMap.<UserRole, Map<UserRole, Set<UserAction>>>builder()
                .put(admin, ImmutableMap.of(adops, ALL_ACTIONS, publisher, ALL_ACTIONS))
                .put(adops, ImmutableMap.of(publisher, ALL_ACTIONS))
                .put(publisher, Collections.emptyMap())
                .build();
    }

    @Override
    public List<AllowedUserRoleActions> getAllowedActions(UserRole role) {
        return userRolesAccessMap.get(role).entrySet().stream()
        .map(entry -> new AllowedUserRoleActions(entry.getKey(), entry.getValue()))
        .collect(Collectors.toList());
    }

    @Override
    public Set<UserAction> getAllowedActions(UserRole subjectRole, UserRole objectRole) {
        return this.userRolesAccessMap.get(subjectRole).get(objectRole);
    }

    @Cacheable("userRoles")
    @Override
    public List<UserRole> getRoles() {
        return userRoleDAO.findAll().stream()
                .map(ModelConvertUtil::convert)
                .collect(Collectors.toList());
    }

}
