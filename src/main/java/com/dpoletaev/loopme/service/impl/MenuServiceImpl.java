package com.dpoletaev.loopme.service.impl;

import com.dpoletaev.loopme.dto.*;
import com.dpoletaev.loopme.service.MenuService;
import com.dpoletaev.loopme.service.UserRoleService;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MenuServiceImpl implements MenuService {
    private static final List<MenuItem> MENU_ITEMS = Arrays.asList(
            new MenuItem("Home", "/"),
            new MenuItem("Users", "/users"),
            new MenuItem("Apps", "/apps")
            );
    private UserRoleService userRoleService;

    public MenuServiceImpl(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    @Override
    public List<MenuItem> getMenuItems(AppUser appUser, String currentURL) {
            return MENU_ITEMS.stream()
                    .filter(menuItem -> !("Users".equals(menuItem.getLabel())) || this.isUsersMenuAvailable(appUser))
                    .filter(menuItem -> !("Apps".equals(menuItem.getLabel()) && appUser.getRole().getName() == UserRole.UserRoleName.ADMIN))
                    .map(this.setSelected(currentURL))
                    .collect(Collectors.toList());
    }

    private Function<MenuItem, MenuItem> setSelected(String currentUrl) {
        return (menuItem -> {
            menuItem.setSelected(isActiveItem(menuItem, currentUrl));
            return menuItem;
        });
    }

    private static boolean isActiveItem(MenuItem menuItem, String currentURL) {
        return stripURL(currentURL).equals(menuItem.getUrl());
    }

    private static String stripURL(String url) {
       return url.replaceAll("^(.*)/?$", "$1");
    }

    private boolean isUsersMenuAvailable(AppUser appUser) {
        List<AllowedUserRoleActions> allowedActions = userRoleService.getAllowedActions(appUser.getRole());

        return !(allowedActions.stream()
                .map(AllowedUserRoleActions::getAllowedActions)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet())
                .isEmpty());
    }
}
