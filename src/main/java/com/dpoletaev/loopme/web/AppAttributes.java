package com.dpoletaev.loopme.web;

public enum AppAttributes {
    APP_USER("app_user"),
    MENU_ITEMS("menu_items"),
    NG_CONTROLLER("ng_controller"),
    NG_TEMPLATE("ng_template");

    private String value;

    AppAttributes(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
