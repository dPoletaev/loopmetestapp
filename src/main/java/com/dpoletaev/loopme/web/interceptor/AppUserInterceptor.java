package com.dpoletaev.loopme.web.interceptor;

import com.dpoletaev.loopme.service.AppUserHolder;
import com.dpoletaev.loopme.web.AppAttributes;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AppUserInterceptor extends HandlerInterceptorAdapter{


    private AppUserHolder appUserHolder;

    public AppUserInterceptor(AppUserHolder appUserHolder) {
        this.appUserHolder = appUserHolder;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute(AppAttributes.APP_USER.value(), appUserHolder.getUser());
        return true;
    }
}
