package com.dpoletaev.loopme.web.interceptor;

import com.dpoletaev.loopme.service.AppUserHolder;
import com.dpoletaev.loopme.service.MenuService;
import com.dpoletaev.loopme.web.AppAttributes;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MenuItemsInterceptor extends HandlerInterceptorAdapter {
    private AppUserHolder appUserHolder;
    private MenuService menuService;

    public MenuItemsInterceptor(AppUserHolder appUserHolder, MenuService menuService) {
        this.appUserHolder = appUserHolder;
        this.menuService = menuService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute(AppAttributes.MENU_ITEMS.value(), menuService.getMenuItems(appUserHolder.getUser(), request.getRequestURI()));
        return true;
    }


}
