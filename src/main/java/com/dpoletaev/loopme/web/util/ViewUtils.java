package com.dpoletaev.loopme.web.util;

import org.springframework.ui.Model;

public final class ViewUtils {
    public static final String CONTENT_TEMPLATE_MODEL_ATTRIBUTE = "contentTemplate";

    public static String view(Model model, String contentTemplate) {
        model.addAttribute(CONTENT_TEMPLATE_MODEL_ATTRIBUTE, contentTemplate);
        return "index";
    }

    private ViewUtils() {
        throw new AssertionError("Utility clazz!");
    }
}
