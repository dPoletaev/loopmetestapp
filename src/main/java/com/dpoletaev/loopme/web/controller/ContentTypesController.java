package com.dpoletaev.loopme.web.controller;

import com.dpoletaev.loopme.dto.ContentType;
import com.dpoletaev.loopme.service.ContentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("data/contentTypes")
public class ContentTypesController {

    @Autowired
    private ContentTypeService contentTypeService;

    @GetMapping
    public List<ContentType> getContentTypes() {
        return contentTypeService.getContentTypes();
    }
}
