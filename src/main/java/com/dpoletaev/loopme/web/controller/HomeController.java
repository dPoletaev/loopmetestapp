package com.dpoletaev.loopme.web.controller;

import com.dpoletaev.loopme.web.util.ViewUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
@RequestMapping("/")
public class HomeController {
    private static final String CONTENT_TEMPLATE = "home";


    @RequestMapping(method = RequestMethod.GET)
    public String view(Model model) {
        return ViewUtils.view(model, CONTENT_TEMPLATE);
    }
}
