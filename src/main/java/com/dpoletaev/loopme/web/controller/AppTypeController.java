package com.dpoletaev.loopme.web.controller;

import com.dpoletaev.loopme.dto.AppType;
import com.dpoletaev.loopme.service.AppTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("data/appTypes")
public class AppTypeController {

    @Autowired
    private AppTypeService appTypeService;

    @GetMapping
    public List<String> getAppTypes() {
        return appTypeService.getAppTypes().stream()
                .map(AppType::name)
                .collect(Collectors.toList());
    }
}
