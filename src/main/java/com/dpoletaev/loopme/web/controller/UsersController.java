package com.dpoletaev.loopme.web.controller;

import com.dpoletaev.loopme.dto.AppUser;
import com.dpoletaev.loopme.service.AppUserHolder;
import com.dpoletaev.loopme.service.UserService;
import com.dpoletaev.loopme.web.util.ViewUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.ContentType;
import java.util.List;

@Controller
@PreAuthorize("hasAnyRole('ROLE_ADOPS','ROLE_ADMIN')")
public class UsersController {
    private static String CONTENT_TEMPLATE = "users";

    @Autowired
    private AppUserHolder appUserHolder;

    @Autowired
    private UserService userService;


    @GetMapping("users")
    public String view(Model model) {
       return ViewUtils.view(model, CONTENT_TEMPLATE);
    }

    @RequestMapping(value = "data/users", method = RequestMethod.GET)
    @ResponseBody
    public List<AppUser> getUsers() {
        return userService.getUsers(appUserHolder.getUser());
    }

    @RequestMapping(value="data/users", method = RequestMethod.POST)
    @ResponseBody
    public AppUser createUser(@RequestBody AppUser appUser) {
        return userService.saveUser(appUser);
    }

    @RequestMapping(value="data/users", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteUser(Integer id) {
        userService.deleteUser(id);
    }

    @RequestMapping(value="data/users", method = RequestMethod.PUT)
    @ResponseBody
    public AppUser updateUser(@RequestBody AppUser appUser) {
        return userService.updateUser(appUser);
    }
}
