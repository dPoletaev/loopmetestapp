package com.dpoletaev.loopme.web.controller;

import com.dpoletaev.loopme.dto.AllowedUserRoleActions;
import com.dpoletaev.loopme.service.AppUserHolder;
import com.dpoletaev.loopme.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserRolesController {

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private AppUserHolder appUserHolder;

    @RequestMapping(value = "/data/roles")
    @ResponseBody
    public List<AllowedUserRoleActions> getAllowedRoles() {
        return userRoleService.getAllowedActions(appUserHolder.getUser().getRole());
    }
}
