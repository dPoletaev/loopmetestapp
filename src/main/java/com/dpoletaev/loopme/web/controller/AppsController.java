package com.dpoletaev.loopme.web.controller;

import com.dpoletaev.loopme.dto.App;
import com.dpoletaev.loopme.dto.AppUser;
import com.dpoletaev.loopme.dto.AppWrapper;
import com.dpoletaev.loopme.dto.UserRole;
import com.dpoletaev.loopme.service.AppService;
import com.dpoletaev.loopme.service.AppUserHolder;
import com.dpoletaev.loopme.web.util.ViewUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@PreAuthorize("hasAnyRole('ROLE_ADOPS','ROLE_PUBLISHER')")
public class AppsController {
    private static String CONTENT_TEMPLATE = "apps";

    @Autowired
    private AppService appService;

    @Autowired
    private AppUserHolder appUserHolder;

    @GetMapping("apps")
    public String view(Model model) {
        return ViewUtils.view(model, CONTENT_TEMPLATE);
    }

    @GetMapping("data/apps")
    @ResponseBody
    public List<AppWrapper> getApps() {
        return appService.getApps(appUserHolder.getUser());
    }

    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @PostMapping("data/apps")
    @ResponseBody
    public AppWrapper saveApp(@RequestBody App app) {
        return appService.create(app, appUserHolder.getUser());
    }

    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @DeleteMapping("data/apps")
    @ResponseBody
    public void deleteApp(Integer id) {
        appService.delete(id);
    }

    @PreAuthorize("hasRole('ROLE_PUBLISHER')")
    @PutMapping("data/apps")
    @ResponseBody
    public AppWrapper updateApp(@RequestBody App app) {
       return appService.edit(app, appUserHolder.getUser());
    }

    @GetMapping("data/canAdd/app")
    @ResponseBody
    public boolean canAdd() {
        return appUserHolder.getUser().getRole().getName() == UserRole.UserRoleName.PUBLISHER;
    }
}
