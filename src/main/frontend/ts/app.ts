import * as angular from "angular";
import "angular-resource";
import "angular-ui-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import "../css/app.scss";

import UsersViewComponent from "./users/UsersView.component";
import UsersTableComponent from "./users/UsersTable.component";
import UsersModalController from "./users/UsersModal.controller";
import {UsersResourceFactory} from "./users/UsersResource"
import {LabelsService} from "./common/service/Labels.service";
import {AllowedUserRolesResourceFactory} from "./common/resource/UserRolesResource";
import GridTableComponent from "./common/components/GridTable.component";
import AppsViewComponent from "./apps/AppsView.component";
import {AppsResourceFactory} from "./apps/AppsResource";
import AppsTableComponent from "./apps/AppsTable.component";
import AppsModalController from "./apps/AppsModal.controller";
import {ContentTypeResourceFactory} from "./apps/ContentTypeResource";
import AppTypeResource from "./apps/AppTypeResource";
import ContentTypeFilter from "./apps/ContentType.filter";
import AppRightsResource from "./apps/AppRightsResource";


const app = angular.module("app", ["ngResource", "ui.bootstrap"]);

app
    .filter("contentTypeFilter", ContentTypeFilter)
    .factory("usersResource", UsersResourceFactory)
    .factory("rolesResource", AllowedUserRolesResourceFactory)
    .factory("appsResource", AppsResourceFactory)
    .factory("contentTypeResource", ContentTypeResourceFactory)
    .service("appTypeResource", AppTypeResource)
    .service("appRightsResource", AppRightsResource)
    .service("labelsService", LabelsService)
    .controller("UsersModalController", UsersModalController)
    .controller("AppsModalController", AppsModalController)
    .component("gridTable", GridTableComponent)
    .component("usersTable", UsersTableComponent)
    .component("usersView", UsersViewComponent)
    .component("appsTable", AppsTableComponent)
    .component("appsView", AppsViewComponent);

