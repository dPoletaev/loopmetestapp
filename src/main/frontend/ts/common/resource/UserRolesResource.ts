
export interface IUserRole {
    id: number;
    name: string;
}

export interface IAllowedUserRolesActions extends ng.resource.IResource<IAllowedUserRolesActions> {
    userRole: IUserRole;
    allowedActions: string[];
}

export interface IAllowedUserRolesResource extends ng.resource.IResourceClass<IAllowedUserRolesActions> {

}

export function AllowedUserRolesResourceFactory($resource: ng.resource.IResourceService): IAllowedUserRolesResource {
    return <IAllowedUserRolesResource> $resource('data/roles');
}

