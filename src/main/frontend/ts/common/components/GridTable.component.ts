import {IController} from "angular";
import {ILabelsService} from "../service/Labels.service";
import {IActionsWrapper} from "../dto/ActionsWrapper";


interface GridTableBindings {
    data: IActionsWrapper<any>[];
    headerKeys: string[];
    valueKeys: string[];
    filters: { [key: string]: string[] };
    onAdd: () => void;
    onAction: (index: number, action: string) => void;
    canAdd: boolean;
}

interface IGridTableController extends GridTableBindings, IController {
    data: IActionsWrapper<any>[];
    headerKeys: string[];
    headers: string[];
    filters: { [key: string]: string[] };
    valueKeys: string [];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;
    canAdd: boolean;
}

class GridTableController implements IGridTableController {
    static $inject = ["labelsService", "$parse", "$filter"];

    data: IActionsWrapper<any>[];
    headerKeys: string[];
    headers: string[];
    filters: { [key: string]: string[] };
    valueKeys: string [];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;
    canAdd: boolean;

    constructor(private labelsService: ILabelsService,
                private $parse: ng.IParseService,
                private $filter: ng.IFilterService) {
    }

    $onInit() {
        this.headers = this.headerKeys.map(this.labelsService.getLabel);
    }

    getValue(obj: any, key: string): any {
        let value = this.$parse(key)(obj);

        if (this.filters && this.filters[key]) {
            this.filters[key].forEach((filter) => {
                    let filterFunc: (any) => any = this.$filter(filter);
                    value = filterFunc(value);
                }
            );
        }

        return value;
    }
}

class GridTableComponent implements ng.IComponentOptions {
    public template: string;
    public bindings: any;
    public controller: any;

    constructor() {
        this.bindings = {
            data: "<",
            headerKeys: "<",
            valueKeys: "<",
            filters: "<",
            canAdd: "<",
            onAdd: "&",
            onAction: "&"
        };
        this.controller = GridTableController;
        this.template = require("./gridTable.template.html");
    }
}

export default new GridTableComponent();