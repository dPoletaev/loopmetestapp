export interface ILabelsService {
    getLabel(key: string): string;
}

export const LABELS = {
    "NAME": "Name",
    "EMAIL": "Email",
    "ROLE": "Role",
    "CREATE_USER": "Create user",
    "EDIT_USER": "Edit user",
    "CREATE_APP": "Create app",
    "EDIT_APP": "Edit app",
    "OK": "Ok",
    "CANCEL": "Cancel",
    "FIELD_REQUIRED": "Field is required",
    "EMAIL_INVALID": "Email is not valid",
    "CONTENT_TYPE": "Content type",
    "APP_TYPE": "App type"
};

//TODO: provide i18n implementation
export class LabelsService implements ILabelsService {

    public getLabel(key: string): string {
        return LABELS[key] || key;
    }
}