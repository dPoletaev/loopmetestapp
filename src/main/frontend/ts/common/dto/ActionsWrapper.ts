export interface IActionsWrapper<T> {
    target: T;
    isEditable: boolean;
    isDeletable: boolean;
}

export class ActionsWrapper<T> implements IActionsWrapper<T> {
    target: T;
    isEditable: boolean;
    isDeletable: boolean;

    constructor(target: T, isEditable: boolean, isDeletable: boolean) {
        this.target = target;
        this.isEditable = isEditable;
        this.isDeletable = isDeletable;
    }
}