import {IUserRole} from "../common/resource/UserRolesResource";

export interface IUser extends ng.resource.IResource<IUser> {
    id: number;
    name: string;
    email: string;
    role: IUserRole;
}

export interface IUsersResource extends ng.resource.IResourceClass<IUser> {
    update(user: IUser): IUser;
}

export function UsersResourceFactory($resource: ng.resource.IResourceService): IUsersResource {
    return <IUsersResource> $resource('data/users', {}, {
        "update": {
            method: "PUT",
            isArray: false
        }
    });
}

