import {IUser, User} from "./User";
import Action from "../common/dto/Action";
import {ILabelsService} from "../common/service/Labels.service";
import {IUserRole} from "../common/resource/UserRolesResource";

export default class UsersModalController {
    static $inject = ["$uibModalInstance", "labelsService", "data"];

    header: string;
    action: string;
    user: IUser;
    userRoles: IUserRole[];

    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalInstanceService,
                public labelsService: ILabelsService,
                data: any) {
        this.user = data.user ? data.user : {};
        this.action = data.action;
        this.userRoles = data.userRoles;

        if(this.userRoles.length && !this.user.role) {
            this.user.role = this.userRoles[0];
        }

        this.header = this.labelsService.getLabel(this.action === Action.UPDATE ? "UPDATE_USER" : "CREATE_USER");
    }

    ok(): void {
        this.$uibModalInstance.close(this.user);
    }

    cancel(): void {
        this.$uibModalInstance.dismiss();
    }


}