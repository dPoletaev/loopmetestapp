
export interface IUserRole {
    id: number;
    name: string;
}

export interface IAllowedUserRoles {
    userRole: IUserRole;
    allowedActions: string[];
}

export interface IUser {
    id: number;
    name: string;
    role: IUserRole;
}

export class User implements IUser {
    id: number;
    name: string;
    role: IUserRole;
}