import {IController} from "angular";
import {IActionsWrapper} from "../common/dto/ActionsWrapper";
import {IUser} from "./UsersResource";


interface UserTableBindings {
   users: IActionsWrapper<IUser>[];
   onAdd: () => void;
   onAction: (index: number, action: string) => void;
}

interface IUsersTableController extends UserTableBindings, IController {

}

class UsersTableController implements IUsersTableController {
    static $inject = [];

    users: IActionsWrapper<IUser>[];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;

    headerKeys: string[] = ["NAME", "EMAIL", "ROLE"];
    dataKeys: string[] = ["name", "email", "role.name"];
    canAdd: boolean = true;

}

class UsersTableComponent implements ng.IComponentOptions {
    public template: string;
    public bindings: any;
    public controller: any;


    constructor() {
        this.bindings = {
            users: "<",
            onAdd: "&",
            onAction: "&"
        };
        this.controller = UsersTableController;
        this.template = require("./usersTable.template.html");
    }
}

export default new UsersTableComponent();