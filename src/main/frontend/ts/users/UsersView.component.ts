import * as angular from "angular";

import {IUserRole} from "./User";
import {IUsersResource, IUser} from "./UsersResource";
import Action from "../common/dto/Action";
import {IAllowedUserRolesActions, IAllowedUserRolesResource} from "../common/resource/UserRolesResource";
import {ActionsWrapper, IActionsWrapper} from "../common/dto/ActionsWrapper";

interface IUsersViewController {
    users: IActionsWrapper<IUser>[];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;

}

class UsersViewController implements IUsersViewController {
    static $inject = ["usersResource", "rolesResource", "$uibModal"];

    private modalOptions: ng.ui.bootstrap.IModalSettings = {
        template: require("./usersModal.template.html"),
        controller: "UsersModalController",
        controllerAs: "$ctrl",
        backdrop: "static",

    };

    private allowedUserRoles: IAllowedUserRolesActions[];

    public users: ActionsWrapper<IUser>[];

    constructor(private usersResource: IUsersResource,
                private rolesResource: IAllowedUserRolesResource,
                private $uibModal: ng.ui.bootstrap.IModalService) {

    }

    $onInit() {
        this.allowedUserRoles = this.rolesResource.query();
        this.allowedUserRoles.$promise.then(
            () => {
                let users: IUser[] = this.usersResource.query();
                users.$promise.then(() => {
                    this.users = users.map(user => this.wrapUserWithAction(user));
                });
            }
        );
    }

    private wrapUserWithAction(user: IUser): IActionsWrapper<IUser> {
        let allowedRolesActions: string[][] = this.allowedUserRoles
            .filter((item: IAllowedUserRolesActions) => item.userRole.id === user.role.id)
            .map((item: IAllowedUserRolesActions) => item.allowedActions);

        let allowedRoles: string[] = allowedRolesActions.length ? allowedRolesActions[0] : [];

        let canEdit = allowedRoles.indexOf(Action.UPDATE) >= 0;
        let canDelete = allowedRoles.indexOf(Action.DELETE) >= 0;

        return new ActionsWrapper(user, canEdit, canDelete);
    }

    onAdd(): void {
        let modalInstance: ng.ui.bootstrap.IModalInstanceService =
            this.$uibModal.open({
                ...this.modalOptions, resolve: {
                    data: () => {
                        return {
                            user: null,
                            action: Action.CREATE,
                            userRoles: this.getAllowedUserRolesForAction(Action.CREATE)
                        }
                    }
                }
            });

        modalInstance.result.then((user: IUser) => {
                let createdUser = this.usersResource.save(user);
                createdUser.$promise.then(() => this.users.push(this.wrapUserWithAction(createdUser)));
            }
        );
    }

    onAction(index: number, action: string) {
        switch(action) {
            case Action.DELETE:
                this.onDelete(index);
                break;
            case Action.UPDATE:
                this.onEdit(index);
                break;
            default: throw new Error("Unknown action");
        }
    }

    private onDelete(index: number) {
        this.usersResource.delete({id: this.users[index].target.id},
            () => this.users.splice(index, 1)
        );
    }

    private onEdit(index: number): void {
        let modalInstance: ng.ui.bootstrap.IModalInstanceService =
            this.$uibModal.open({
                ...this.modalOptions, resolve: {
                    data: () => {
                        return {
                            user: angular.copy(this.users[index].target),
                            action: Action.UPDATE,
                            userRoles: this.getAllowedUserRolesForAction(Action.CREATE)
                        }
                    }
                }
            });

        modalInstance.result.then((user: IUser) => {
            let updatedUser = this.usersResource.update(user);
            updatedUser.$promise.then(() => this.users[index] = this.wrapUserWithAction(updatedUser));
        });
    }

    public getAllowedUserRolesForAction(action: string): IUserRole[] {
        let result = [];
        this.allowedUserRoles.forEach((allowedActions: IAllowedUserRolesActions) => {
            if (allowedActions.allowedActions.indexOf(action) >= 0) {
                result.push(allowedActions.userRole);
            }
        });
        return result;
    }

}

class UsersViewComponent implements ng.IComponentOptions {
    public template: string;
    public controller: any;


    constructor() {
        this.template = require("./usersView.template.html");
        this.controller = UsersViewController;
    }


}

export default new UsersViewComponent();