import Action from "../common/dto/Action";
import {ILabelsService} from "../common/service/Labels.service";
import {IApp} from "./AppsResource";
import {IContentType, IContentTypeResource} from "./ContentTypeResource";
import {IAppTypeResource} from "./AppTypeResource";

export default class AppsModalController {
    static $inject = ["$uibModalInstance", "labelsService", "contentTypeResource", "appTypeResource", "data"];

    header: string;
    action: string;
    app: IApp;
    contentTypes: IContentType[];
    contentTypesSelected: { [key: number]: boolean };
    appTypes: string[];

    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalInstanceService,
                public labelsService: ILabelsService,
                private contentTypeResource: IContentTypeResource,
                private appTypeResource: IAppTypeResource,
                data: any) {

        this.app = data.app ? data.app : {};
        this.action = data.action;


        this.contentTypeResource.query().$promise.then((contentTypes: IContentType[]) => {
            this.contentTypes = contentTypes || [];
            this.contentTypesSelected = this.constructContentTypesSelected(contentTypes, this.app.contentTypes);
        });

        this.appTypeResource.query().then((appTypes: string[]) => {
            this.appTypes = appTypes;});

        this.header = this.labelsService.getLabel(this.action === Action.UPDATE ? "UPDATE_APP" : "CREATE_APP");
    }




    private constructContentTypesSelected(contentTypes: IContentType[], appContentTypes?: IContentType[]): { [key: number]: boolean } {
        let result = {};

        contentTypes.forEach((contentType: IContentType) =>
            result[contentType.id] = false);
        if(appContentTypes) {
            appContentTypes.forEach((contentType) => result[contentType.id] = true);
        }
        return result;
    }

    ok(): void {
        this.setContentTypes(this.app);
        this.$uibModalInstance.close(this.app);
    }

    private setContentTypes(app: IApp): void {
        app.contentTypes = this.contentTypes.filter(item => this.contentTypesSelected[item.id]);
    }

    cancel(): void {
        this.$uibModalInstance.dismiss();
    }



}