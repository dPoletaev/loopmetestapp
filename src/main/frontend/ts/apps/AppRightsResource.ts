import {IHttpResponse, IHttpService, IPromise} from "angular";

export interface IAppRightsResource {
    canAdd(): IPromise<boolean>

}

export default class AppRightsResource {

    static $inject = ["$http"];

    constructor(private httpService: IHttpService) {

    }

    canAdd(): IPromise<boolean> {
        return this.httpService.get<boolean>("data/canAdd/app")
            .then((response: IHttpResponse<boolean>) => response.data);
    }
}