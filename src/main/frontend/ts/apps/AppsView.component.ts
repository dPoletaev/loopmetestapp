import {ActionsWrapper, IActionsWrapper} from "../common/dto/ActionsWrapper";
import {IApp, IAppsResource, IAppWrapper} from "./AppsResource";
import Action from "../common/dto/Action";
import * as angular from "angular";


interface IAppsViewController {
    tableData: IActionsWrapper<IApp>[];

    onAction(index: number, action: string): void;

    onAdd(): void;
}

class AppsViewController implements IAppsViewController {
    static $inject = ["appsResource", "$uibModal"];

    public tableData: IActionsWrapper<IApp>[];

    private modalOptions: ng.ui.bootstrap.IModalSettings = {
        template: require("./appsModal.template.html"),
        controller: "AppsModalController",
        controllerAs: "$ctrl",
        backdrop: "static",
    };

    constructor(private appsResource: IAppsResource,
                private $uibModal: ng.ui.bootstrap.IModalService) {
        this.tableData = [];
    }

    $onInit() {
        this.appsResource.query().$promise.then((apps: IAppWrapper[]) => {
            this.tableData = this.tableData.concat(apps.map((app) =>
                new ActionsWrapper(app.app, app.editable, app.deletable)));
        });
    }

    onAdd(): void {
        let modalInstance: ng.ui.bootstrap.IModalInstanceService =
            this.$uibModal.open({
                ...this.modalOptions, resolve: {
                    data: () => {
                        return {
                            app: null,
                            action: Action.CREATE
                        }
                    }
                }
            });

        modalInstance.result.then((app: IApp) => {
                let createdApp = this.appsResource.save(app);
                createdApp.$promise.then(() => this.tableData.push(this.transformToTableData(createdApp)));
            }
        );
    }

    onAction(index: number, action: string): void {
        switch(action) {
            case Action.DELETE:
                this.onDelete(index);
                break;
            case Action.UPDATE:
                this.onEdit(index);
                break;
            default: throw new Error("Unknown action");
        }    }

    private onDelete(index: number) {
        this.appsResource.delete({id: this.tableData[index].target.id},
            () => this.tableData.splice(index, 1)
        );
    }

    private onEdit(index: number): void {
        let modalInstance: ng.ui.bootstrap.IModalInstanceService =
            this.$uibModal.open({
                ...this.modalOptions, resolve: {
                    data: () => {
                        return {
                            app: angular.copy(this.tableData[index].target),
                            action: Action.UPDATE
                        }
                    }
                }
            });

        modalInstance.result.then((app: IApp) => {
            let updatedApp = this.appsResource.update(app);
            updatedApp.$promise.then(() => this.tableData[index] = this.transformToTableData(updatedApp));
        });

    }


    private transformToTableData(appWrapper: IAppWrapper): IActionsWrapper<IApp> {
        return appWrapper && new ActionsWrapper(appWrapper.app, appWrapper.editable, appWrapper.deletable);
    }

}

class AppsViewComponent implements ng.IComponentOptions {
    public template: string;
    public controller: any;

    constructor() {
        this.template = require("./appsView.template.html");
        this.controller = AppsViewController;
    }


}

export default new AppsViewComponent();