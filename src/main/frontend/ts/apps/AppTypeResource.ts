import {IHttpResponse, IHttpService, IPromise} from "angular";

export interface IAppTypeResource {
    query(): IPromise<any>

}

export default class AppTypeResource {

    static $inject = ["$http"];

    constructor(private httpService: IHttpService) {

    }

    query(): IPromise<string[]> {
        return this.httpService.get<string[]>("data/appTypes")
            .then((response: IHttpResponse<string[]>) => response.data);
    }
}