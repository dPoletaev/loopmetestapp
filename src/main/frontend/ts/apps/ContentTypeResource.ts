
export interface IContentType extends ng.resource.IResource<IContentType> {
    id: number;
    contentTypeName: string;
}

export interface IContentTypeResource extends ng.resource.IResourceClass<IContentType> {

}

export function ContentTypeResourceFactory($resource: ng.resource.IResourceService): IContentTypeResource {
    return <IContentTypeResource> $resource('data/contentTypes');
}