import {IController} from "angular";
import {IActionsWrapper} from "../common/dto/ActionsWrapper";
import {IApp} from "./AppsResource";
import {IAppRightsResource} from "./AppRightsResource";


interface AppsTableBindings {
    apps: IActionsWrapper<IApp>[];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;
}

interface IAppsTableController extends AppsTableBindings, IController {
    apps: IActionsWrapper<IApp>[];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;
    filters: { [key: string]: string[] };
    canAdd: boolean;
}

class AppsTableController implements IAppsTableController {
    static $inject = ["appRightsResource"];

    apps: IActionsWrapper<IApp>[];
    onAdd: () => void;
    onAction: (index: number, action: string) => void;

    headerKeys: string[] = ["NAME", "APP_TYPE", "CONTENT_TYPE"];
    dataKeys: string[] = ["name", "appType", "contentTypes"];
    filters: { [key: string]: string[] } = {
        "contentTypes": ["contentTypeFilter"]
    };
    canAdd: boolean = false;

    constructor(private appRightsResource: IAppRightsResource) {
    }


    $onInit(): void {
        this.appRightsResource.canAdd().then((canAdd: boolean) => {
                this.canAdd = canAdd;
            }
        );
    }
}

class AppsTableComponent implements ng.IComponentOptions {
    public template: string;
    public bindings: any;
    public controller: any;
    onAdd: () => void;
    onAction: (index: number, action: string) => void;


    constructor() {
        this.bindings = {
            apps: "<",
            onAdd: "&",
            onAction: "&"
        };
        this.controller = AppsTableController;
        this.template = require("./appsTable.template.html");
    }
}

export default new AppsTableComponent();