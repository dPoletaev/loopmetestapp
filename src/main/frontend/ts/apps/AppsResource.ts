import {IContentType} from "./ContentTypeResource";
import {IUser} from "../users/UsersResource";

export interface IApp {
    id: number;
    name: string;
    appType: string;
    contentTypes: IContentType[];
    user: IUser;
}

export interface IAppWrapper extends ng.resource.IResource<IAppWrapper> {
    app: IApp;
    editable: boolean;
    deletable: boolean;
}

export interface IAppsResource extends ng.resource.IResourceClass<IAppWrapper> {
    update: (app: IApp) => IAppWrapper
}

export function AppsResourceFactory($resource: ng.resource.IResourceService): IAppsResource {
    return <IAppsResource> $resource('data/apps', {}, {
        "update": {
            method: "PUT",
            isArray: false
        }
    });
}

