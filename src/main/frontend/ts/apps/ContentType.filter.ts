import {IContentType} from "./ContentTypeResource";

export default function () {
    return (input: IContentType[]): string => {
        if (!input) {
            return null;
        }

        let result =
            input.map(contentType => contentType.contentTypeName)
                .join(", ");

        return result;
    }

}