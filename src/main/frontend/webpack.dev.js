const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const PATHS = {
    app: path.join(__dirname, "ts", "app.ts"),
    bundlePath: path.join(__dirname, '..', "webapp", "resources", "bundle"),
    cssFilePath: path.join("..", "bundle", "app.css")
};


module.exports = {
    entry: {
        app: PATHS.app,
    },
    output: {
        path: PATHS.bundlePath,
        filename: '[name].js'
    },
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: {loader: "ts-loader"}
            },
            {
                test: /\.html$/,
                use: {loader: "html-loader"}
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: "url-loader", query: {
                        limit: 10000,
                        mimetype: "application/font-woff"
                    }
                }
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: "url-loader", query: {
                        limit: 10000,
                        mimetype: "application/octet-stream"
                    }
                }
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: "file-loader"
                }
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: "url-loader", query: {
                        limit: 10000,
                        mimetype: "image/svg+xml"
                    }
                }
            },
            {
                test: /\.s?css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{loader: 'css-loader', options: {importLoaders: 1}},
                        'postcss-loader']
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin(PATHS.cssFilePath)]
};