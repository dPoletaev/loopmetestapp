module.exports = {
    parser: false,
    plugins: {
        'postcss-import': {},
        'cssnext': {},
        'autoprefixer': {},
        'cssnano': {},
        "postcss-nested": {}
    }
};